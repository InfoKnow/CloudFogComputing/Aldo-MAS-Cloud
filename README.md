MAS-Cloud
=============

**MAS-Cloud** is a **M**ulti-**A**gent **S**ystem

A multiagent system to dynamically monitor, predict and provide computational resources in cloud platforms. Deductive reasoning agents work cooperatively in a three layer architecture to provide transparent horizontal elasticity of virtual machines in public cloud platforms (i.e., Google, Amazon EC2). MAS-Cloud was evaluated with a nondeterministic and CPU intensive application providing a challenging validation case. 

## The MAS-Cloud architecture

![The MAS-Cloud architecture](http://portalprogramando.com.br/wp-content/uploads/2018/09/ArchitectureProject.png)

## Requirements:
- Java (7+) - A programming language that produces software for multiple platforms.
- A Linux with DSTAT installed.
- Drools (6.5) - is a inference based rules engine
- A set of libraries available at lib folder.
