﻿package Starter;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import models.ModelsConfigMachines;

public class PriceGoogleCloud {
	private final static String USER_AGENT = "Mozilla/5.0";

  public static void prices() {
	  
		String jsonString = callURL("https://cloudpricingcalculator.appspot.com/static/data/pricelist.json");
		jsonString="[ "+jsonString+" ]";
		
		
		//System.out.println(jsonString);

//Replace this try catch block for all below subsequent examples
		try {  

			JSONArray jsonArray = new JSONArray(jsonString);

			JSONObject row = jsonArray.getJSONObject(0);
			row = row.getJSONObject("gcp_price_list");
			
			System.out.println(row.getJSONObject("CP-COMPUTEENGINE-CUSTOM-VM-CORE").toString());
			System.out.println(row.getJSONObject("CP-COMPUTEENGINE-CUSTOM-VM-RAM").toString());
			
			JSONObject CPU=row.getJSONObject("CP-COMPUTEENGINE-CUSTOM-VM-CORE");
			JSONObject Mem=row.getJSONObject("CP-COMPUTEENGINE-CUSTOM-VM-RAM");
			
			
			Double europeCpu=Double.parseDouble(CPU.get("europe").toString());
			Double asiaCpu=Double.parseDouble(CPU.get("asia").toString());
			Double usCpu=Double.parseDouble(CPU.get("us").toString());
			
			if(europeCpu<asiaCpu && europeCpu<usCpu){
				Starter.prices.setCpuPrice(europeCpu/60);
				((ModelsConfigMachines) Starter.prices).setLocalMem("europe");
			}else if(asiaCpu<europeCpu && asiaCpu<usCpu){
				Starter.prices.setCpuPrice(asiaCpu/60);
				((ModelsConfigMachines) Starter.prices).setLocalCpu("asia");
			}else {
				Starter.prices.setCpuPrice(usCpu/60);
				Starter.prices.setLocalMem("us");
			}
			
			Double europeMem=Double.parseDouble(Mem.get("europe").toString());
			Double asiaMem=Double.parseDouble(Mem.get("asia").toString());
			Double usMem=Double.parseDouble(Mem.get("us").toString());
			
			if(europeMem<asiaMem && europeMem<usMem){
				Starter.prices.setMemoryPrice(europeMem/60);
				Starter.prices.setLocalMem("europe");
			}else if(asiaMem<europeMem && asiaMem<usMem){
				Starter.prices.setMemoryPrice(asiaMem/60);
				Starter.prices.setLocalMem("asia");
			}else {
				Starter.prices.setMemoryPrice(usMem/60);
				Starter.prices.setLocalMem("us");
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		System.out.println("preço "+Starter.prices.getCpuPrice());
	}

	public static String callURL(String myURL) {
		System.out.println("Requested URL:" + myURL);
		StringBuilder sb = new StringBuilder();
		HttpURLConnection urlConn = null;
		InputStreamReader in = null;
		URL url=null;
		try {
			try {
				url = new URL("https://cloudpricingcalculator.appspot.com/static/data/pricelist.json");
			} catch (MalformedURLException e) {e.printStackTrace();}
			try {
			    urlConn = (HttpURLConnection) url.openConnection(); 
			} catch (IOException e) { 
				e.printStackTrace(); 
			}
			urlConn.setRequestProperty("User-Agent", USER_AGENT);
			urlConn.setRequestProperty("Accept-Language", "iso-8859-1");
			urlConn.setDoInput(true);
			urlConn.setRequestMethod("GET");
			urlConn.setRequestProperty("Content-length", "0");
			urlConn.setUseCaches(false);
			urlConn.setAllowUserInteraction(false);
			urlConn.connect();
			int status = urlConn.getResponseCode();
			InputStream inputStream = new FileInputStream("https://cloudpricingcalculator.appspot.com/static/data/pricelist.json");
			if (urlConn != null)
				urlConn.setReadTimeout(60 * 1000);
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(),
						Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
				if (bufferedReader != null) {
					int cp;
					 BufferedReader br = new BufferedReader(new InputStreamReader(inputStream,"iso-8859-1"));
		                sb = new StringBuilder();
		                String line;
		                while ((line = br.readLine()) != null) {
		                    sb.append(line+"\n");
		                }
		                br.close();
		                return sb.toString();
				}
			}
		in.close();
		
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL:"+ myURL, e);
		} 

		return sb.toString();
	}

}